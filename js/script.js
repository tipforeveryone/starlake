(function($) {
    Drupal.behaviors.myBehavior = {
        attach: function (context, settings) {

            //FUNCTIONS

                /*LIST OF FUNCTIONS
                responsive_slideshow(classname,original_img_width,original_img_height);
                counter_stylist(id_of_li_element)
                scroll_fading_show_up(element,speed,delay,offset)
                */

                //Responsive Slideshow
                function responsive_slideshow(
                    classname, //Tên class của block slideshow
                    original_img_width, //Chiều rộng gốc của hình sử dụng trong slideshow
                    original_img_height //Chiều cao gốc của hình sử dụng trong slideshow
                ){
                    //Xác định chiều cao của slideshow (responsive) dựa trên tỉ lệ định trước của hình slide
                    var image_ratio = original_img_width / original_img_height;
                    var control_button_height = $(classname + ' .views-slideshow-controls-text-next').height();
                    if($(window).width() < 992){
                        var fieldset_height = $(classname + ' .views-fieldset').height();
                        var slide_height = $(classname).parent().width() / image_ratio + fieldset_height;
                        var control_button_top = (slide_height - fieldset_height) / 2 - control_button_height / 2;
                    }else{
                        var slide_height = $(classname).parent().width() / image_ratio;
                        var control_button_top = slide_height / 2 - control_button_height / 2;
                    }
                    //Định vị lại các thành phần của sliedshow
                    $(classname + ' .views-slideshow-cycle-main-frame').css("height",slide_height);
                    $(classname + ' .views-slideshow-controls-text-next').css("top",control_button_top);
                    $(classname + ' .views-slideshow-controls-text-previous').css("top",control_button_top);
                    $(classname + ' .views-slideshow-pager-fields').css("top",slide_height - 35);
                }

                //Visitor module counter stylist
                function counter_stylist(id_of_li_element){
                    var str0 = $('.visitor > ul > li').eq(id_of_li_element).text();
                    var str1 = str0.slice(0,str0.indexOf(":") + 1);
                    var str2 = str0.slice(str0.indexOf(":") + 2,str0.length);
                    //Fill up with zero numbers
                    if(str2.length < 7){
                        var group_of_zero = "0";
                        var loop_time = 7 - str2.length;
                        for(var x=1; x < loop_time; x++){
                            group_of_zero = group_of_zero + "0"
                        }
                        str2 = group_of_zero + str2;
                    }
                    $('.visitor > ul > li').eq(id_of_li_element).html(
                        "<div class='counter-label'>" + str1 + "</div>" +
                        "<div class='counter-number'>" + str2 + "</div>"
                    );
                }

            //END OFF FUNCTIONS

            //CODE STARTS HERE
                //Tính toán chiều cao cho slideshow responsive
                responsive_slideshow(".home-slideshow",1200,356);
                //adjust_parent_height(".home-duanphanphoi .view-footer",".view");
                $(window).resize(function(){
                    responsive_slideshow(".home-slideshow",1200,356);
                });

                //Điều khiển nút switch user
                $(".block-masquerade .description").html($(".block-masquerade .description").children());
                $(".block-masquerade").hover(function(){
                    $(this).stop().animate({left:0},200).animate({opacity:1},200);
                },function(){
                    $(this).stop().animate({left:-150},200) .animate({opacity:0.5});
                });

                //Điều khiển khối menu quản trị onpage
                $(".menu-onpage .menu li .dropdown-toggle").html("");
                $(".menu-onpage").hover(function(){
                    $(this).stop().animate({opacity:1},200);
                },function(){
                    //var block_width = $(this).width() + 10;
                    $(this).stop().animate({opacity:0.5},200);
                });

                $duration = 800;

                $('.front .banner-top-content').hover(function(){
                    $('.front .banner-top-content').stop().animate({backgroundColor: "rgba(255,255,255,1)"}, $duration);
                },function(){
                    $('.front .banner-top-content').stop().animate({backgroundColor: "rgba(255,255,255,0)"}, $duration);
                });

                //Hieu ung dua chuot vao mot trong 3 khoi banner
                $('.home-lienket .view-content .views-row').hover(function(){
                    $(this).not('.clicked').stop().animate({flexBasis:"34%"},{duration: $duration - 500, queue: false});
                    $(this).siblings('.views-row').not('.clicked').stop().animate({flexBasis:"33%"},{duration: $duration - 500, queue: false});

                    $(this).children('.black').animate({opacity:0.1},{duration: $duration - 500, queue: false});
                    $(this).siblings('.views-row').children('.black').stop().animate({opacity:0.7},{duration: $duration - 500, queue: false});

                    $(this).children('.title').children('a').stop().animate({backgroundColor:"rgba(0,0,0,0.6)"},{duration: $duration - 500, queue: false});
                    $(this).siblings('.views-row').children('.title').children('a').stop().animate({backgroundColor:"rgba(0,0,0,0)"},{duration: $duration - 500, queue: false});
                },function(){
                });

                $('.home-lienket').hover(function(){},function(){
                    $('.home-lienket .view-content .views-row').stop().animate({flexBasis:"33.333%"}, $duration);
                    $('.home-lienket .view-content .views-row .black').stop().animate({opacity:0.3}, $duration);
                    $('.home-lienket .view-content .views-row .title a').stop().animate({backgroundColor:"rgba(0,0,0,0)"},{duration: $duration, queue: false});
                    $('.home-lienket .view-content .views-row').removeClass('.clicked');
                });

                $('.home-lienket .view-content .views-row').click(function(){
                    $(this).stop().animate({flexBasis:"56%"},{duration: $duration - 200, queue: false});
                    $(this).siblings('.views-row').stop().animate({flexBasis:"22%"},{duration: $duration - 200, queue: false});
                    $(this).addClass('expanded');
                    $(this).siblings().removeClass('expanded');
                    $('.home-lienket .view-content .views-row').addClass('clicked');
                });

                //Animation effect
                $(".home-lienket .views-row-1").addClass("wow animated fadeIn slow").attr("data-wow-delay","0.7s");
                $(".home-lienket .views-row-2").addClass("wow animated fadeIn slow").attr("data-wow-delay","1.4s");
                $(".home-lienket .views-row-3").addClass("wow animated fadeIn slow").attr("data-wow-delay","2.1s");

                /*
                $(".home-thuvien > .title").addClass("wow animated bounceInUp").attr("data-wow-delay","0.0s");
                $(".home-tintuc > .title").addClass("wow animated bounceInUp").attr("data-wow-delay","0.0s");

                $(".home-video .views-row .infogroup").addClass("wow animated bounceInUp").attr("data-wow-delay","0.0s");
                $(".home-video .views-row .views-field-body").addClass("wow animated bounceInUp").attr("data-wow-delay","0.3s");

                $(".home-album .views-row-1").addClass("wow animated bounceInUp").attr("data-wow-delay","0.0s");
                $(".home-album .views-row-2").addClass("wow animated bounceInUp").attr("data-wow-delay","0.3s");
                $(".home-album .views-row-3").addClass("wow animated bounceInUp").attr("data-wow-delay","0.6s");

                $(".home-tinmoinhat").addClass("wow animated bounceInUp").attr("data-wow-delay","0.0s");

                $(".home-baiviet .views-row-1").addClass("wow animated bounceInUp").attr("data-wow-delay","0.0s");
                $(".home-baiviet .views-row-2").addClass("wow animated bounceInUp").attr("data-wow-delay","0.3s");
                $(".home-baiviet .views-row-3").addClass("wow animated bounceInUp").attr("data-wow-delay","0.6s");
                $(".home-baiviet .views-row-4").addClass("wow animated bounceInUp").attr("data-wow-delay","0.9s");

                $(".footerinfo").addClass("wow animated bounceInUp").attr("data-wow-delay","0.0s");
                $(".footerinfo-contact").addClass("wow animated bounceInUp").attr("data-wow-delay","0.3s");
                $(".menufooter").addClass("wow animated bounceInUp").attr("data-wow-delay","0.6s"); */

            //CODE ENDS HERE
        }
    };
})
(jQuery);

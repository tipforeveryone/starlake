(function($) {
    Drupal.behaviors.myBehavior = {
        attach: function (context, settings) {

            //CODE STARTS HERE

            $("#block-system-navigation h2").click(function(){
                $("#block-system-navigation .content > ul").toggle();
                $(window).scrollTop(0);
            });

            //Điều khiển nút switch user
            $(".block-masquerade .description").html($(".block-masquerade .description").children());
            $(".block-masquerade").hover(function(){
                $(this).stop().animate({left:0},200).animate({opacity:1},200);
            },function(){
                $(this).stop().animate({left:-150},200) .animate({opacity:0.7},200);
            });

            //CODE ENDS HERE
        }
    };
})
(jQuery);
